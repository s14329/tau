package pl.edu.pjwstk.gdansk.tau.service;

import pl.edu.pjwstk.gdansk.tau.model.Car;

import java.util.List;

public interface ICarService {

    void setEnableTimeOnAddCar(boolean enableTimeOnAddCar);
    void setEnableTimeOnReadCar(boolean enableTimeOnReadCar);
    void setEnableTimeOnUpdateCar(boolean enableTimeOnUpdateCar);

    void create(Car car);
    List<Car> readAll();
    Car read (int carId);
    void update(Car car);
    void delete(Car car);
}
