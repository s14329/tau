package pl.edu.pjwstk.gdansk.tau.model;

import java.time.LocalDateTime;
import java.util.Objects;

public class Car {
    private int id;
    private String manufacturer;
    private String model;
    private double engineDisplacement; //unit 'cbm' - Cubic Decimeters
    private int price; //unit 'PLN' - Polish Złoty

    private LocalDateTime readTime;
    private LocalDateTime addTime;
    private LocalDateTime updateTime;

    public Car() {
    }

    public Car(String manufacturer, String model, double engineDisplacement, int price) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.engineDisplacement = engineDisplacement;
        this.price = price;
    }

    public Car(int id,String manufacturer, String model, double engineDisplacement, int price) {
        this.id = id;
        this.manufacturer = manufacturer;
        this.model = model;
        this.engineDisplacement = engineDisplacement;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getEngineDisplacement() {
        return engineDisplacement;
    }

    public void setEngineDisplacement(double engineDisplacement) {
        this.engineDisplacement = engineDisplacement;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public LocalDateTime getReadTime() {
        return readTime;
    }

    public void setReadTime(LocalDateTime readTime) {
        this.readTime = readTime;
    }

    public LocalDateTime getAddTime() {
        return addTime;
    }

    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return id == car.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
