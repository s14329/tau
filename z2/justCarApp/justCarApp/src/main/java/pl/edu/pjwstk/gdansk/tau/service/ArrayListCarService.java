package pl.edu.pjwstk.gdansk.tau.service;

import pl.edu.pjwstk.gdansk.tau.model.Car;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.time.Clock;
import java.time.LocalDateTime;

public class ArrayListCarService implements ICarService{

    private boolean enableTimeOnReadCar = true;
    private boolean enableTimeOnAddCar = true;
    private boolean enableTimeOnUpdateCar = true;
    private Clock clock;

    private List<Car> cars = new ArrayList<Car>();

    public ArrayListCarService() {
        clock = Clock.systemDefaultZone();
    }

    public ArrayListCarService(Clock clock) {
        this.clock = clock;
    }

    public void create(Car car) {
        car.setId(generateId());
        if (enableTimeOnAddCar) {
            car.setAddTime(LocalDateTime.now(clock));
        }
        cars.add(car);
    }

    public List<Car> readAll() {
        cars.stream().forEach(car-> car.setReadTime(LocalDateTime.now(clock)));
        return cars;
    }

    public Car read(int carId) {
        Car readCar = cars
                .stream()
                .filter(car -> car.getId() == carId)
                .findFirst()
                .orElseThrow(NoSuchElementException::new)
                ;
        if (enableTimeOnReadCar) {
            readCar.setReadTime(LocalDateTime.now(clock));
        }
        return readCar;
    }

    public void update(Car car) {
        Car carInMemory = read(car.getId());

        carInMemory.setManufacturer(car.getManufacturer());
        carInMemory.setModel(car.getModel());
        carInMemory.setEngineDisplacement(car.getEngineDisplacement());
        carInMemory.setPrice(car.getPrice());
        if(enableTimeOnUpdateCar) {
            carInMemory.setUpdateTime(LocalDateTime.now(clock));
        }
    }

    public void delete(Car car) {
        cars.removeIf(carInMemory -> carInMemory.getId() == car.getId());
    }

    private int generateId() {
        return cars.size()+1;
    }

    public boolean isEnableTimeOnReadCar() {
        return enableTimeOnReadCar;
    }

    public void setEnableTimeOnReadCar(boolean enableTimeOnReadCar) {
        this.enableTimeOnReadCar = enableTimeOnReadCar;
    }

    public boolean isEnableTimeOnAddCar() {
        return enableTimeOnAddCar;
    }

    public void setEnableTimeOnAddCar(boolean enableTimeOnAddCar) {
        this.enableTimeOnAddCar = enableTimeOnAddCar;
    }

    public boolean isEnableTimeOnUpdateCar() {
        return enableTimeOnUpdateCar;
    }

    public void setEnableTimeOnUpdateCar(boolean enableTimeOnUpdateCar) {
        this.enableTimeOnUpdateCar = enableTimeOnUpdateCar;
    }
}
