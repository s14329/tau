package pl.edu.pjwstk.gdansk.tau;

import org.junit.Before;
import org.junit.Test;
import pl.edu.pjwstk.gdansk.tau.model.Car;
import pl.edu.pjwstk.gdansk.tau.service.ArrayListCarService;
import pl.edu.pjwstk.gdansk.tau.service.ICarService;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/*
    service.create(new Car("Volkswagen", "Golf", 2.0, 15000));
    service.create(new Car("Volkswagen", "Polo", 1.0, 10000));
    service.create(new Car("Volkswagen", "Touareg", 3.0, 50000));
 */

public class TimestampArrayListCarServiceTests {
    LocalDateTime timestamp;
    ICarService service;

    @Before
    public void setup() {
        timestamp = LocalDateTime.of(2019,2,6,12,00);
        service = new ArrayListCarService(Clock.fixed(timestamp.toInstant(ZoneOffset.UTC),ZoneId.of("UTC")));
    }

    @Test
    public void testAddingNewCarWithTime() {
        Car addedCar = new Car("Volkswagen", "Golf", 2.0, 15000);
        service.create(addedCar);

        assertEquals(timestamp, addedCar.getAddTime());
    }

    @Test
    public void testGettingAllCars() {
        service.create(new Car("Volkswagen", "Golf", 2.0, 15000));
        service.create(new Car("Volkswagen", "Polo", 1.0, 10000));
        service.create(new Car("Volkswagen", "Touareg", 3.0, 50000));

        for(Car car : service.readAll()) {
            assertEquals(timestamp,car.getReadTime());
        }
    }

    @Test
    public void testGettingOneCarFromMemory() {
        Car addedCar = new Car("Volkswagen", "Golf", 2.0, 15000);
        service.create(addedCar);

        assertEquals(timestamp,addedCar.getAddTime());
    }

    @Test
    public void testNotSetReadTimeWhenGettingCar() {
        service.setEnableTimeOnReadCar(false);

        Car addedCar = new Car("Volkswagen", "Golf", 2.0, 15000);
        service.create(addedCar);

        assertNotNull(addedCar);
        assertNull(addedCar.getReadTime());
    }

    @Test
    public void testNotSetAddTimeWhenGettingCar() {
        service.setEnableTimeOnAddCar(false);

        Car addedCar = new Car("Volkswagen", "Golf", 2.0, 15000);
        service.create(addedCar);

        assertNotNull(addedCar);
        assertNull(addedCar.getAddTime());
    }

    @Test
    public void testSetNotUpdateTimeWhenGettingDiet() {
        service.setEnableTimeOnUpdateCar(false);

        Car addedCar = new Car("Volkswagen", "Golf", 2.0, 15000);
        service.create(addedCar);

        assertNotNull(addedCar);
        assertNull(addedCar.getUpdateTime());
    }

    @Test
    public void testSetReadTimeWhenGettingCar() {
        service.setEnableTimeOnReadCar(true);

        Car addedCar = new Car("Volkswagen", "Golf", 2.0, 15000);
        service.create(addedCar);

        assertNotNull(addedCar);
        assertEquals(timestamp,service.read(1).getReadTime());
    }

    @Test
    public void testSetAddTimeWhenGettingCar() {
        service.setEnableTimeOnAddCar(true);

        Car addedCar = new Car("Volkswagen", "Golf", 2.0, 15000);
        service.create(addedCar);

        assertNotNull(addedCar);
        assertEquals(timestamp,service.read(1).getAddTime());
    }

    @Test
    public void testSetUpdateTimeWhenGettingDiet() {
        service.setEnableTimeOnUpdateCar(true);

        Car addedCar = new Car("Volkswagen", "Golf", 2.0, 15000);
        Car updateCar = new Car(1,"Volkswagen", "Polo", 1.0, 10000);

        service.create(addedCar);
        service.update(updateCar);

        assertNotNull(addedCar);
        assertEquals(timestamp,service.read(1).getUpdateTime());
    }
}
