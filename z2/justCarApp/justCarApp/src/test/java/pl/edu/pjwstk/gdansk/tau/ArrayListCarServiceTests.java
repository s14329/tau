package pl.edu.pjwstk.gdansk.tau;

import org.junit.Before;
import org.junit.Test;
import pl.edu.pjwstk.gdansk.tau.model.Car;
import pl.edu.pjwstk.gdansk.tau.service.ArrayListCarService;
import pl.edu.pjwstk.gdansk.tau.service.ICarService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertAll;

public class ArrayListCarServiceTests {

    private ICarService service;

    @Before
    public void prepair() {
        service = new ArrayListCarService();
    }

    @Test
    public void testCreatingNewCarInMemory() {
        service.create(new Car("Volkswagen", "Golf", 2.0, 15000));

        assertNotNull(service.readAll().stream().findFirst().get());
    }

    @Test
    public void testIdGeneratingOnNewCarAdding() {
        service.create(new Car("Volkswagen", "Golf", 2.0, 15000));

        assertNotNull(service.readAll().stream().findFirst().get().getId());
    }

    @Test
    public void testCarInformationCorrectionAfterAdding() {
        service.create(new Car("Volkswagen", "Golf", 2.0, 15000));

        Car car = service.readAll().stream().findFirst().get();

        assertAll("car",
                () -> assertEquals("Volkswagen", car.getManufacturer()),
                () -> assertEquals("Golf", car.getModel()),
                () -> assertEquals(2.0, car.getEngineDisplacement(),0.01),
                () -> assertEquals(15000, car.getPrice())
                );
    }

    @Test
    public void testReadingAllCarsInMemory() {
        service.create(new Car("Volkswagen", "Golf", 2.0, 15000));
        service.create(new Car("Volkswagen", "Polo", 1.0, 10000));
        service.create(new Car("Volkswagen", "Touareg", 3.0, 50000));

        assertEquals(3,service.readAll().size());
    }

    @Test
    public void testReadingSpecificCarInMemory() {
        service.create(new Car("Volkswagen", "Golf", 2.0, 15000));

        assertNotNull(service.read(1));
    }

    @Test
    public void testUpdatingSpecificCarInMemory() {
        service.create(new Car("Volkswagen", "Golf", 2.0, 15000));
        service.create(new Car("Volkswagen", "Polo", 1.0, 10000));

        service.update(new Car(1,"Audi","A4",1.9,10000));

        Car car = service.read(1);

        assertAll("car",
                () -> assertEquals("Audi",car.getManufacturer()),
                () -> assertEquals("A4",car.getModel()),
                () -> assertEquals(1.9,car.getEngineDisplacement(),0.1),
                () -> assertEquals(10000,car.getPrice())
                );
    }

    @Test
    public void testDeletingSpecificCarInMemory() {
        service.create(new Car("Volkswagen", "Golf", 2.0, 15000));
        service.create(new Car("Volkswagen", "Polo", 1.0, 10000));

        Car car = service.readAll().stream().findFirst().get();

        service.delete(car);

        assertEquals(1,service.readAll().size());
    }
}