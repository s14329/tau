package pl.edu.pjwstk.gdansk.tau.service;

import pl.edu.pjwstk.gdansk.tau.model.Car;

import java.util.List;

public interface ICarService {
    void create(Car car);
    List<Car> readAll();
    Car read (int carId);
    void update(Car car);
    void delete(Car car);
}
