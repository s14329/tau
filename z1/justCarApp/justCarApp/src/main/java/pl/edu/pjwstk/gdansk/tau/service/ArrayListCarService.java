package pl.edu.pjwstk.gdansk.tau.service;

import pl.edu.pjwstk.gdansk.tau.model.Car;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ArrayListCarService implements ICarService{

    private List<Car> cars = new ArrayList<Car>();

    public void create(Car car) {
        car.setId(generateId());
        cars.add(car);
    }

    public List<Car> readAll() {
        return cars;
    }

    public Car read(int carId) {
        return cars
                .stream()
                .filter(car -> car.getId() == carId)
                .findFirst()
                .orElseThrow(NoSuchElementException::new)
                ;
    }

    public void update(Car car) {
        Car carInMemory = read(car.getId());

        carInMemory.setManufacturer(car.getManufacturer());
        carInMemory.setModel(car.getModel());
        carInMemory.setEngineDisplacement(car.getEngineDisplacement());
        carInMemory.setPrice(car.getPrice());

    }

    public void delete(Car car) {
        cars.removeIf(carInMemory -> carInMemory.getId() == car.getId());
    }

    private int generateId() {
        return cars.size()+1;
    }
}
