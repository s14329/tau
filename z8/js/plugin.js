(function( $ ) {
    $.fn.validate = function() {
    	this.each(function() {
    		let element = this;
    		let regex = /^[a-z0-9]{3,10}$/;
            let value = $(element).val();
			let classes = element.className.split(' ');
            let result = regex.test(value);
            if (!result) {classes.push('invalid');}
            else {classes.push('valid');};
            element.className = classes.join(' ');
    	});
    	return this;
    };
 
}( jQuery ));