describe("validate", function () {

    beforeEach(function () {
        let s = spyOn(console, 'log').and.callThrough();
        $('body').append(`
            <br>
            <form id="form">
              <input type="text" class="validate1" value="xyz" />
              <input type="text" class="validate2" value="XYZ" />
              <input type="text" class="validate3" value="123" />
              <button>Send</button>
            </form>
            <br>
        `);
    });

    afterEach(function () {
        $('body').append(`
            <hr>
            `);
    });

    it("Should pass validation.", function () {
        $('.validate1').validate();
        let field = document.querySelector('.validate1');
        expect(field).toHaveClass('valid');
    });

    it("Should not pass validation.", function () {
        $('.validate2').validate();
        let field = document.querySelector('.validate2');
        expect(field).toHaveClass('invalid');
    });

    it("Should pass and not pass validation.", function () {
        $('.validate1').validate();
        $('.validate2').validate();

        let validate1 = document.querySelector('.validate1');
        let validate2 = document.querySelector('.validate2');

        expect(validate1).toHaveClass('valid');
        expect(validate2).toHaveClass('invalid');
    });    

    it("Should validate field.", function () {
        $('.validate1').change(validateField).keyup(validateField);;
        $('.validate2').change(validateField).keyup(validateField);;
        $('.validate3').change(validateField).keyup(validateField);;

        function validateField() {
            $(this).validate();
        }
    });
});